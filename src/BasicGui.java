
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class BasicGui {

    private JFrame frame;
    private JTextField Number1, Number2, Total;
    public ScCalc c = new ScCalc();

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    BasicGui window = new BasicGui();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void ShowRes() {
        Total.setText("" + c.total);
    }

    /**
     * Create the application.
     */
    public BasicGui() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 450, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        Number1 = new JTextField();
        Number1.setBounds(318, 65, 86, 20);
        frame.getContentPane().add(Number1);
        Number1.setColumns(10);

        Number2 = new JTextField();
        Number2.setBounds(318, 108, 86, 20);
        frame.getContentPane().add(Number2);
        Number2.setColumns(10);

        Total = new JTextField();
        Total.setBounds(318, 174, 86, 20);
        frame.getContentPane().add(Total);
        Total.setColumns(10);

        JButton AddButton = new JButton("Add +");
        AddButton.setFont(new Font("Tahoma", Font.PLAIN, 12));
        AddButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                c.add(Integer.parseInt(Number1.getText()), Integer.parseInt(Number2.getText()));
                ShowRes();
            }
        });
        AddButton.setBounds(10, 62, 89, 23);
        frame.getContentPane().add(AddButton);

        JButton MinusButton = new JButton("Subtract -");
        MinusButton.setFont(new Font("Tahoma", Font.PLAIN, 12));
        MinusButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                c.subtract(Integer.parseInt(Number1.getText()), Integer.parseInt(Number2.getText()));
                ShowRes();
            }
        });
        MinusButton.setBounds(109, 63, 89, 23);
        frame.getContentPane().add(MinusButton);

        JButton MultiplyButton = new JButton("Multiply X");
        MultiplyButton.setFont(new Font("Tahoma", Font.PLAIN, 12));
        MultiplyButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                c.multiply(Integer.parseInt(Number1.getText()), Integer.parseInt(Number2.getText()));
                ShowRes();
            }
        });
        MultiplyButton.setBounds(10, 105, 89, 23);
        frame.getContentPane().add(MultiplyButton);

        JButton DivideButton = new JButton("Divide /");
        DivideButton.setFont(new Font("Tahoma", Font.PLAIN, 12));
        DivideButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                c.divide(Integer.parseInt(Number1.getText()), Integer.parseInt(Number2.getText()));
                ShowRes();
            }
        });
        DivideButton.setBounds(109, 105, 89, 23);
        frame.getContentPane().add(DivideButton);

        JLabel lblNewLabel = new JLabel("Number 1");
        lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
        lblNewLabel.setBounds(233, 68, 60, 14);
        frame.getContentPane().add(lblNewLabel);

        JLabel lblNumber = new JLabel("Number 2");
        lblNumber.setFont(new Font("Tahoma", Font.PLAIN, 12));
        lblNumber.setBounds(233, 111, 60, 14);
        frame.getContentPane().add(lblNumber);

        JLabel lblTotal = new JLabel("Total");
        lblTotal.setForeground(Color.RED);
        lblTotal.setFont(new Font("Tahoma", Font.BOLD, 14));
        lblTotal.setBounds(233, 177, 68, 14);
        frame.getContentPane().add(lblTotal);

        JLabel lblNewLabel_1 = new JLabel("Basic Calculator");
        lblNewLabel_1.setFont(new Font("Lucida Console", Font.ITALIC, 18));
        lblNewLabel_1.setBounds(98, 11, 206, 30);
        frame.getContentPane().add(lblNewLabel_1);

        JButton SquareButton = new JButton("Square");
        SquareButton.setFont(new Font("Tahoma", Font.PLAIN, 12));
        SquareButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                c.square(Integer.parseInt(Number1.getText()));
                ShowRes();
            }
        });
        SquareButton.setBounds(10, 147, 89, 23);
        frame.getContentPane().add(SquareButton);

        JButton RootButton = new JButton("Square Root");
        RootButton.setFont(new Font("Tahoma", Font.PLAIN, 10));
        RootButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                c.root(Integer.parseInt(Number1.getText()));
                ShowRes();
            }
        });
        RootButton.setBounds(109, 146, 89, 23);
        frame.getContentPane().add(RootButton);

		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Options");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Exit");
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		mnNewMenu.add(mntmNewMenuItem_1);
	}
    }
    

