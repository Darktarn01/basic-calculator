

public class Calc {
    int a, b, total;

    int subtract(int a, int b) {
        total = a - b;
        return total;
    }

    int add(int a, int b) {
        total = a + b;
        return total;
    }

    int multiply(int a, int b) {
        total = a * b;
        return total;
    }

    int divide(int a, int b) {
        total = a / b;
        return total;
    }

    int total() {
        return total;
    }

}